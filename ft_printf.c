/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 15:12:45 by ftothmur          #+#    #+#             */
/*   Updated: 2019/09/16 23:22:29 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

void			print_options(t_list *head)
{
	t_option	*option;
	while (head)
	{
		option = (t_option *)(head->content);
		printf(	""
				"isoption:   %s\n"
				"flags:     |%i|%i|%i|%i|%i|\n"
				"width:      %i\n"
				"precision:  %i\n"
				"length:    |%c|\n"
				"type:      |%c|\n"
				"frmt subs  |%.*s|\n"
				"subs size   %zi\n"
				"arg_size    %zi\n"
				"ptrarg      %p\n"
				"fltarg      %Lf\n"
				"intarg      %ju\n"
				"_________\n\n",
				!option->isoption ? "IS_OPTION" : "NO_OPTION",
				(int)(*(option->flags + SPACE)),
				(int)(*(option->flags + OCTOTHORPE)),
				(int)(*(option->flags + PLUS)),
				(int)(*(option->flags + MINUS)),
				(int)(*(option->flags + ZERO)),
				option->width,
				option->precision,
				option->length,
				option->type,
				(int)option->substr_size,
				option->format_substr,
				option->substr_size,
				option->arg_size,
				(void *)(option->u_arg.intmax),
				option->u_arg.ldbl,
				option->u_arg.intmax);
		if (!(head = head->next))
			break ;
	}
}

void			read_by_arg(va_list varg, t_option *option)
{
	static char	ldouble[SIZE_DBL_NBR + 1] = SIZEOF_DBL;

	if (ft_strchr(ldouble, option->type))
	{
		if (option->length == 'L')
			option->u_arg.ldbl = va_arg(varg, long double );
		else
			option->u_arg.ldbl = (long double)va_arg(varg, double);
	}
	else if (option->type != '%')
		option->u_arg.intmax = va_arg(varg, uintmax_t);
	return ;
}

void			read_args(va_list varg, t_list *head)
{
	t_option	*option;

	while (head)
	{
		option = (t_option *)(head->content);
		if (option->isoption == IS_OPTION)
		{
			if (option->width == IS_SET_BY_ARG)
				if ((option->width = va_arg(varg, int)) < 0)
				{
					*(option->flags + MINUS) = FLAG_FOUND;
					option->width = (((unsigned int)(option->width)
								<< REMOVE_SIGN) >> REMOVE_SIGN);
				}
			if (option->precision == IS_SET_BY_ARG)
					if ((option-> precision = va_arg(varg, int) < 0))
						option->precision = PREC_ISNT_SET;
			if (option->precision != PREC_ISNT_SET)
				*(option->flags + ZERO) = FLAG_NOT_FOUND;
			read_by_arg(varg, option);
		}
		head = head->next;
	}
	return ;
}

/*
void			print_types(t_list *node)
{
	t_option	*option;

	option = (t_option *)(node->content);
	ft_putnstr(option->format_substr, option->substr_size);
	if (option->isoption == IS_OPTION)
	{
		
	}
	if (option->is
	return ;
}*/

int				ft_printf(const char *format, ...)
{
	t_list		*head;
	va_list		varg;
//	int			counter;

	head = NULL;

	int i = -1;
	printf("|%1i|\t|%1i|\n\n", i, (unsigned)i );

	if (parse(&head, format) == FAILURE)
		return (ERROR);
	va_start(varg, format);
	read_args(varg, head);
	print_options(head);
	print_integers((t_option *)head->content);
//	print_str((t_option *)head->content);
//	print_chars(varg, (t_option *)(head->content));

	ft_lstdel(&head, ft_lstcontentdel);
	va_end(varg);
	return (SUCCESS);
}

int				main(void)
{
	int			i;
	
	ft

	i = -4;
	ft_printf(""
			// formt str
			"123121%30.20i"/*
			"%*.*i"
			"90%%"
			"abcdefghijk%Lg"*/,
			// args
			-11/*,
			i, i, i,

			2.3L*/
			);
	return (SUCCESS);
}
