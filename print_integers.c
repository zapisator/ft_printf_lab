/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_integers.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/15 14:26:20 by ftothmur          #+#    #+#             */
/*   Updated: 2019/09/16 23:01:05 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				fit_integer_by_sign(t_option *option)
{
	if (option->length == 'h' + 'h')
		option->issign = ft_remove_sign(option->u_arg.chars, CHAR_BYTES);
	else if (option->length == 'h' + 'h')
		option->issign = ft_remove_sign(option->u_arg.chars, SHORT_BYTES);
	else if (option->length == '\0')
		option->issign = ft_remove_sign(option->u_arg.chars, INT_BYTES);
	else
		option->issign = ft_remove_sign(option->u_arg.chars, INTMAX_BYTES);
	return ;
}

void				fit_integer_by_qualification(t_option *option)
{
	char			t;
	unsigned char	l;

	t = option->type;
	l = option->length;
	if ((t == 'c' && l != 'l') || (ft_strchr(OP_S_INT OP_U_INT, t) &&
					l == 'h' + 'h'))
		ft_bzero((void *)(option->u_arg.chars + CHAR_BYTES),
				INTMAX_BYTES - CHAR_BYTES);
	else if (ft_strchr(OP_S_INT OP_U_INT, t) && l == 'h')
		ft_bzero((void *)(option->u_arg.chars + SHORT_BYTES),
				INTMAX_BYTES - SHORT_BYTES);
	else if ((t == 'c' && l == 'l') || t == 'C' ||
			(ft_strchr(OP_S_INT OP_U_INT, t) && (l == '\0')))
		ft_bzero((void *)(option->u_arg.chars + INT_BYTES),
				INTMAX_BYTES - SHORT_BYTES);
	if (ft_strchr(OP_S_INT, option->type))
		fit_integer_by_sign(option);	
	return ;
}

int					get_radix(t_option *option)
{
	int				radix;
	
	radix = 10;
	if (ft_strchr("bB", option->type))
		radix = 2;
	else if (ft_strchr("oO", option->type))
		radix = 8;
	else if (ft_strchr("AXax", option->type))
		radix = 16;
	return (radix);
}

void				set_octothorpe(t_option *option)
{
	if (ft_strchr("Bb", option->type))
		ft_strcpy(option->octothorpe, "0b");
	else if (ft_strchr("Oo", option->type))
		ft_strcpy(option->octothorpe, "o");
	else if (ft_strchr("Xpx", option->type))
		ft_strcpy(option->octothorpe, "0x");
	if (ft_strchr("BOX", option->type))
		ft_strtoupper(option->octothorpe);
	return ;
}

void				rectify_nbr_params(t_option *option)
{
	if (option->type == 'p' || (*(option->flags + OCTOTHORPE) &&
				option->u_arg.intmax))
		set_octothorpe(option);
	if (option->precision == PREC_ISNT_SET)
	   option->precision = option->u_arg.intmax ? 1 : 0;
	option->precision = ft_imax(option->precision, option->own_nbr_len);
	option->arg_size = ft_imax(option->issign + ft_strlen(option->octothorpe) +
		option->precision, option->width);
	return ;
}

void				print_sign__octothorpe__precision_zeros(t_option *option)
{
	int				zero_len;
	char			zeros[BEST_SIZE + 1];
	t_udiv			divs;

	if (option->issign)
	   ft_putchar('-');
	else if (*(option->flags + PLUS))
	   ft_putchar('+');
	else if (*(option->flags + SPACE))
		ft_putchar(' ');
	ft_putstr(option->octothorpe);
	ft_memset(zeros, (int)'0', BEST_SIZE);
	*(zeros + BEST_SIZE) = '\0';
	if ((zero_len = option->precision - option->own_nbr_len) > 0)
	{
		divs = ft_udiv_struc(zero_len, BEST_SIZE);
		while (divs.quot--)
			ft_putstr(zeros);
		ft_putnstr(zeros, divs.rem);
	}
	return ;
}

void				print_integers(t_option *option)
{
	char			digits[INTMAX_BITS + 1];
	char			*end_of_nbr;
	unsigned int	radix;

	fit_integer_by_qualification(option);
	radix = get_radix(option);
	end_of_nbr = ft_uimaxtostr(digits, *((uintmax_t *)option->u_arg.chars),
			radix);
	rectify_nbr_params(option);
	if (!*(option->flags + MINUS))
		print_spaces(option, option->arg_size - (option->issign +
				ft_strlen(option->octothorpe) + option->precision));
	print_sign__octothorpe__precision_zeros(option);
	if (option->type == 'X')
		ft_strtoupper(digits);
	ft_putstr(digits);
	if (!*(option->flags + MINUS))
		print_spaces(option, option->arg_size - (option->issign +
				ft_strlen(option->octothorpe) + option->precision));
	return ;
}

