/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 22:41:30 by ftothmur          #+#    #+#             */
/*   Updated: 2019/09/16 22:59:33 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

/*
char			*ft_uimaxtostr(char *str, uintmax_t nbr, t_ui radix)
{
	t_uimaxdiv	div_r;
	char		*radix_cyphers;
	char		*zero;

	if (!str || radix == 1 || radix > 36)
	{
		if (str)
			*str = '\0';
		return (str);
	}
	if (!radix)
		radix = 10;
	zero = str;
	radix_cyphers = "0123456789abcdefghijklmnopqrstuvwxyz";
	div_r = ft_uimaxdiv(nbr, radix);
	while (div_r.quot)
	{
		*zero++ = *(radix_cyphers + div_r.rem);
		div_r = ft_uimaxdiv(div_r.quot, radix);
	}
	*zero++ = *(radix_cyphers + div_r.rem);
	*zero = '\0';
	ft_strrev(str);
	return (zero);
}*/

int				main(void)
{
	static char	ar[5000];

	ft_uimaxtostr(ar, 15, 10);
	printf("str:\n%s\n", ar);
	return (0);
}
