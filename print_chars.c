/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_chars.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 15:11:03 by ftothmur          #+#    #+#             */
/*   Updated: 2019/09/16 20:59:27 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					print_spaces(t_option *option, size_t len)
{
	char				c;

	c = (*(option->flags + ZERO) && !*(option->flags + MINUS)) ? '0' : ' ';
	while (len--)
		ft_putchar(c);
	return ;
}

void				print_wstr(t_option *option)
{
	wchar_t			*str;
	
	if (!(str = (wchar_t *)option->u_arg.intmax))
		str = L"(null)";
	option->precision = option->precision == PREC_ISNT_SET ? ft_wcslen(str) :
		ft_imin(option->precision, ft_wcslen(str));
	option->arg_size = (ssize_t)ft_imax(option->width, option->precision);
	if (!*(option->flags + MINUS))
		(void)print_spaces(option, option->arg_size - option->precision);
	ft_putnwcs(str, option->precision);
	if (*(option->flags + MINUS))
		(void)print_spaces(option, option->arg_size - option->precision);
	return ;
}

void				print_str(t_option *option)
{
	char			*str;

	if (option->length == 'l' || option->type == 'S')
		return (print_wstr(option));
	if (!(str = (char *)option->u_arg.intmax))
		str = "(null)";
	option->precision = option->precision == PREC_ISNT_SET ? ft_strlen(str) :
		ft_imin(option->precision, ft_strlen(str));
	option->arg_size = (ssize_t)ft_imax(option->width, option->precision);
	if (!*(option->flags + MINUS))
		(void)print_spaces(option, option->arg_size - option->precision);
	ft_putnstr(str, option->precision);
	if (*(option->flags + MINUS))
		(void)print_spaces(option, option->arg_size - option->precision);
	return ;
}

void				print_wint(t_option *option)
{
	int				len;
	wchar_t			c;
	
	c = (wchar_t)option->u_arg.intmax;
	len = ft_wclen(c);
	if (!*(option->flags + MINUS))
		print_spaces(option, len);
	ft_putwchar(c);
	if (*(option->flags + MINUS))
		print_spaces(option, len);
	return ;
}

void				print_chars(t_option *option)
{
	int				len;
	char			c;

	if (option->length == 'l' || option->type == 'C')
		return (print_wint(option));
	c = (char)option->u_arg.intmax;
	len = sizeof(char);
	if (!*(option->flags + MINUS))
		print_spaces(option, len);
	ft_putchar(c);
	if (*(option->flags + MINUS))
		print_spaces(option, len);
	return ;
}
